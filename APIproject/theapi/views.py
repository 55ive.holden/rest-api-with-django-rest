from rest_framework import viewsets
from .serializers import WorkerSerializer
from .models import Worker


class WorkerViewSet(viewsets.ModelViewSet):
    queryset = Worker.objects.all().order_by('name')
    serializer_class = WorkerSerializer
