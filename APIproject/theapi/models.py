from django.db import models


class Worker(models.Model):
    name = models.CharField(max_length=60)
    position = models.CharField(max_length=60)

    def __str__(self):
        return self.name
